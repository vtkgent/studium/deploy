# Studium (deployment)

Studium is an online platform for exchanging summaries, tips and learning
experiences.

Studium is free software. See the files whose names start with COPYING for
copying permission.

Copyright years on Studium source files may be listed using range notation,
e.g., 2020-2025, indicating that every year in the range, inclusive, is a
copyrightable year that could otherwise be listed individually.

### Setup servers

```shell script
$ ansible-playbook -u <user> setup.yml
```