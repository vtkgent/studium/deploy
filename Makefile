SHELL := /bin/bash

# Make docker-compose use the docker command-line interface for BuildKit support
export COMPOSE_DOCKER_CLI_BUILD := 1
# Use BuildKit to optimize builds
export DOCKER_BUILDKIT := 1
# Make sure to use the OverlayFS driver
export DOCKER_DRIVER := overlay2

# Get cache from development by default
export CI_PROJECT_PATH ?= vtkgent/site
export CI_REGISTRY_IMAGE ?= registry.gitlab.com/$(CI_PROJECT_PATH)

# Deploy next by default
export VERSION ?= next

# Versions other than latest and next are not supported
ifeq ($(findstring $(VERSION),latest next),)
$(error invalid version "$(VERSION)" for deployment, allowed versions are "latest" and "next")
endif

# Use unprefixed domain for latest
ifeq ($(VERSION),latest)
export DOMAIN = studium.gent
else
# <hash>.studium.gent for testing is not supported yet, but next can be used
export DOMAIN = $(VERSION).studium.gent
endif


all: deploy
.PHONY: all

docker-login:
	@ echo "Validating docker login ..."
	@ { grep "registry.gitlab.com" "$(HOME)/.docker/config.json" && docker login registry.gitlab.com; } > /dev/null 2>&1 || { echo -e "\033[0;31merror: Docker is not logged in, please run 'docker login registry.gitlab.com'.\033[0m" && false; }
	@ echo -e "\033[0;32mDocker login valid!\033[0m"
.PHONY: docker-login

up:
	@ [[ "$$(hostname)" == "studium" ]] || { echo "Error: Do not run this stack on your own pc because it won't work" && exit 1; }
	@ $(info deploying version "$(VERSION)" on "$(DOMAIN)")
	@ docker-compose pull
	@ docker network create -d overlay --attachable traefik >/dev/null 2>&1 || true
	@ docker stack deploy --compose-file docker-compose.traefik.yml studium_traefik
	@ docker stack deploy --compose-file docker-compose.yml studium_${VERSION}
.PHONY: up

deploy:
	@ # Create remote host directories
	@ ssh studium mkdir -p .docker deploy
	@ # Copy docker login
	@ scp $(HOME)/.docker/config.json studium:.docker
	@ # Copy docker-compose files
	@ scp *.yml Makefile studium:deploy
	@ scp -r config/ studium:deploy
	@ # Update docker stack on remote host and cleanup stale images
	@ ssh studium 'make VERSION=$(VERSION) -C deploy up && docker image prune -af'
.PHONY: deploy
